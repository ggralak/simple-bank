Simple Bank Web Application
===========================

Very basic Teller app which can do following actions:

- Create an Account
- Search for an Account using Account Holder's name
- View details of the Account (together with the list of all Transactions)
- Make Lodgemente
- Make Withdrawal (with balance watching)
- Make Transfer between two accounts

To run:
	
	mvn clean install
	cd front-end
	mvn jetty:run
	go to: http://localhost:8080/simple-bank 
	(click search to get a list of sample clients)

Features and technology changes I would add later
-------------------------------------------------

Below are only few ideas I had during development:

### Features

- Add time of Account creation
- Validate Account during creation if one with given ID already exists
- Add format validators for Account's ID & phone no
- Enhance Account's Address - introduce separate fields for street, city etc. (either using Account table or separate one)
- Introduce overdraft so Account's balance can be below 0
- Editing Accounts
- Add validations when money are being entered (like lodgement, withdrawal & transfer)
- Add Account Balance after Transaction (on the list of the Transactions) - for better readability
- Show only recent Transactions on the Account page
- Create separate 'All Transactions' section with paging 
- Add proper formatting of the Dates

### Technology

- Use Spring Security for login/authentication purposes
- Introduce different types of Transactions on the Transaction's Object Type level (use discriminator to differentiate between TransactionTypes)