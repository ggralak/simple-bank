<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row">
	<div class="large-12 columns">
		<h3>Account Details (${account.id})</h3>
		<table>
			<tr>
				<td>Id</td>
				<td>${account.id}</td>
			</tr>
			<tr>
				<td>Name</td>
				<td>${account.name}</td>
			</tr>
			<tr>
				<td>Address</td>
				<td>${account.address}</td>
			</tr>
			<tr>
				<td>Phone No</td>
				<td>${account.phoneNo}</td>
			</tr>
			<tr>
				<td>Balance</td>
				<td>${account.balance}</td>
			</tr>
		</table>
	</div>
</div>

<div class="row">
	<div class="large-12 columns">
		<c:url value="/account/${account.id}/lodgement" var="lodgementUrl"/>
		<c:url value="/account/${account.id}/withdrawal" var="withdrawalUrl"/>
		<c:url value="/account/${account.id}/transfer" var="transferUrl"/>
		<a href="${lodgementUrl}" class="button small round">Lodgement</a>
		<a href="${withdrawalUrl}" class="button small round">Withdrawal</a>
		<a href="${transferUrl}" class="button small round">Transfer</a>
	</div>
</div>

<div class="row">
	<div class="large-12 columns">
		<h3>All transactions:</h3>
		<table>
			<thead>
				<tr>
				<th>Id</th>
				<th>Amount</th>
				<th>Date</th>
				<th>Type</th>
				<th>To Account</th>
				<th>From Account</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="transaction" items="${transactions}">
					<tr>
						<td>${transaction.id}</td>
						<td>${transaction.amount}</td>
						<td>${transaction.date}</td>
						<td>${transaction.type}</td>
						<td>${transaction.toAccount.id}</td>
						<td>${transaction.fromAccount.id}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>