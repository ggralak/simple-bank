<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row">
	<div class="large-12 columns">
		<h3>Create Account</h3>
	</div>
</div>

<div class="row">
	<div class="large-12 columns">
		<c:url value="/account/new" var="createAccountUrl"/>
		<form:form method="POST" commandName="account" action="${createAccountUrl}">
			
					<div class="row collapse">
        				<div class="small-2 columns">
        			  		<form:label path="id">Id:</form:label>
        			  		<font color='red'><form:errors path='id' /></font>
        				</div>
        				<div class="small-10 columns">
          					<form:input path="id"></form:input>
        				</div>
      				</div>
					
					<div class="row collapse">
        				<div class="small-2 columns">
        			  		<form:label path="name">Name:</form:label>
        			  		<font color='red'><form:errors path='name' /></font>
        				</div>
        				<div class="small-10 columns">
          					<form:input path="name"></form:input>
        				</div>
      				</div>
      				
      				<div class="row collapse">
        				<div class="small-2 columns">
        			  		<form:label path="address">Address:</form:label>
        			  		<font color='red'><form:errors path='address' /></font>
        				</div>
        				<div class="small-10 columns">
          					<form:input path="address"></form:input>
        				</div>
      				</div>
      				
      				<div class="row collapse">
        				<div class="small-2 columns">
        			  		<form:label path="phoneNo">Phone No:</form:label>
        			  		<font color='red'><form:errors path='phoneNo' /></font>
        				</div>
        				<div class="small-10 columns">
          					<form:input path="phoneNo"></form:input>
        				</div>
      				</div>
      						
			<button type="submit" class="button round">Create Account</button>
		</form:form>
	</div>
</div>