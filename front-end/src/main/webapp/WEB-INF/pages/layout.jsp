<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
        <title>tiles:insertAttribute name="title" ignore="true"/></title>
        <link rel="stylesheet" href="<c:url value="/resources/stylesheets/app.css" />" />
        <script src="<c:url value="/resources/javascripts/vendor/custom.modernizr.js" />"></script>
    </head>
    <body>

        <div class="row">
            <div class="large-12 columns">
                <tiles:insertAttribute name="header"/>
            </div>
        </div>
        <div class="row">
            <div class="large-12 columns">
                <tiles:insertAttribute name="content"/>
            </div>
        </div>

        <script>
            document.write('<script src=' + ('__proto__' in {} ? '<c:url value="/resources/javascripts/vendor/zepto" />' : '<c:url value="/resources/javascripts/vendor/jquery" />') + '.js><\/script>')
        </script>

        <script src="<c:url value="/resources/javascripts/foundation/foundation.js" />"></script>
        <script src="<c:url value="/resources/javascripts/foundation/foundation.alerts.js" />"></script>
        <script src="<c:url value="/resources/javascripts/foundation/foundation.clearing.js" />"></script>
        <script src="<c:url value="/resources/javascripts/foundation/foundation.cookie.js" />"></script>
        <script src="<c:url value="/resources/javascripts/foundation/foundation.dropdown.js" />"></script>
        <script src="<c:url value="/resources/javascripts/foundation/foundation.forms.js" />"></script>
        <script>
            $(document).foundation();
        </script>
    </body>
</html>