<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row">
	<div class="large-12 columns">
		<c:url value="/account/new" var="createAccount"/>
		<a href="${createAccount}"class="button">Create Account</a>
	</div>
</div>


<div class="row">
	<div class="large-12 columns">
		<c:url value="/account/search" var="searchUrl"/>
		<form:form method="POST" action="${searchUrl}">
			<div class="row">
				<div class="large-6 columns">
					<div class="row collapse">
        				<div class="small-10 columns">
          					<input name="name" type="text" placeholder="Search accounts by name" />
        				</div>
        				<div class="small-2 columns">
        			  		<button type="submit" class="button prefix">Search</button>
        				</div>
      				</div>
				</div>
			</div>
		</form:form>
	</div>
</div>

<div class="row">
	<div class="large-12 columns">
		<h3>Results</h3>
		<table>
			<thead>
				<tr>
				<th>Id</th>
				<th>Name</th>
				<th>Address</th>
				<th>Phone No</th>
				<th>Balance</th>
				<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="account" items="${accounts}">
					<tr>
						<td>${account.id}</td>
						<td>${account.name}</td>
						<td>${account.address}</td>
						<td>${account.phoneNo}</td>
						<td>${account.balance}</td>
						<c:url value="/account/${account.id}" var="accountUrl"/>
						<td><a href="${accountUrl}" class="button round small">View</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>