<%@taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row">
	<div class="large-12 columns">
		<c:url value="/" var="baseUrl"/>
		<h2><a href="${baseUrl}">Simple Bank</a></h2>
		<hr />
	</div>
</div>

<div class="row">
	<div class="large-12 columns">
		<c:if test="${ERR_MESSAGE != null}">
  		<div data-alert class="alert-box alert">
  			${ERR_MESSAGE}
  		</div>
		</c:if>
	</div>
</div>

<div class="row">
	<div class="large-12 columns">
		<c:if test="${FLASH_MESSAGE != null}">
  		<div data-alert class="alert-box secondary">
  			${FLASH_MESSAGE}
  		</div>
		</c:if>
	</div>
</div>