<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row">
	<div class="large-12 columns">
		<h3>Make Withdrawal</h3>	
	</div>
</div>

<div class="row">
	<div class="large-12 columns">
		<c:if test="${FLASH_MESSAGE != null}">
  		<div data-alert class="alert-box secondary">
  			${FLASH_MESSAGE}
  		</div>
		</c:if>
	</div>
</div>

<div class="row">
	<div class="large-12 columns">
		<h3>Account balance: ${account.balance}</h3>
		<c:url value="/account/${account.id}/withdrawal" var="transactionUrl"/>
		<form:form method="POST" action="${transactionUrll}">
			<div class="row">
				<div class="large-6 columns">
					<div class="row collapse">
        				<div class="small-10 columns">
          					<input name="amount" type="text" placeholder="How much to withdraw?" />
        				</div>
        				<div class="small-2 columns">
        			  		<button type="submit" class="button prefix">Make Withdrawal</button>
        				</div>
      				</div>
				</div>
			</div>
		</form:form>
	</div>
</div>

