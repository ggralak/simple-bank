package me.gralak.validators;

import me.gralak.model.Account;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Very simple validator. 
 * Created only for future so more sophisticated validation can be added here.
 * 
 * @author Grzegorz Gralak
 */
@Component
public class AccountValidator implements Validator {
	
	@Override
	public boolean supports(Class clazz) {
		return Account.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors e) {
		ValidationUtils.rejectIfEmpty(e, "id", "validation.empty");
		ValidationUtils.rejectIfEmpty(e, "name", "validation.empty");
		ValidationUtils.rejectIfEmpty(e, "address", "validation.empty");
		ValidationUtils.rejectIfEmpty(e, "phoneNo", "validation.empty");
	}
}
