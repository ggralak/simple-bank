package me.gralak.controllers;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class MainController {
	Logger log = Logger.getLogger(MainController.class.getName());
		
	@RequestMapping(value="", method=RequestMethod.GET)
	public String showIndex(){
		return "index";
	}	

}
