package me.gralak.controllers;

import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;
import javax.validation.Valid;

import me.gralak.enums.TransactionType;
import me.gralak.exceptions.BalanceException;
import me.gralak.model.Account;
import me.gralak.service.AccountService;
import me.gralak.validators.AccountValidator;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping(value = "account")
public class AccountController {
	Logger log = Logger.getLogger(AccountController.class.getSimpleName());

	@Autowired
	protected MessageSource messageSource;

	@Resource
	@Qualifier("accountService")
	protected AccountService accountService;

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(new AccountValidator());
	}

	/**
	 * Search for an account by name, show main page with results
	 * 
	 * @param name
	 *            - name which will be used in Accounts search
	 * @param model
	 *            - results will be saved there
	 * @return index page
	 */
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public String search(@RequestParam("name") String name, ModelMap model) {
		log.debug("Search for accounts using name = " + name);

		List<Account> accounts = accountService.findAccountByName(name);
		model.addAttribute("accounts", accounts);

		log.debug("Found " + accounts.size() + " accounts.");
		return "index";
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String showAccount(ModelMap model, @PathVariable("id") String id) {
		// Get account based on the id
		Account account = this.accountService.getAccount(id);
		model.addAttribute("account", account);
		// Get all transactions for the given account
		model.addAttribute("transactions", account.getTransactions());
		return "account";
	}

	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public ModelAndView showCreateAccount() {
		return new ModelAndView("new-account", "account", new Account());
	}

	@RequestMapping(value = "/new", method = RequestMethod.POST)
	public ModelAndView createAccount(@ModelAttribute @Valid Account account,
			BindingResult result) {
		log.debug("Creating Account with name = " + account.getName() + ", id="
				+ account.getId());

		ModelAndView modelAndView = new ModelAndView();

		if (result.hasErrors()) {
			modelAndView.setViewName("new-account");
			return modelAndView;
		}

		// No errors - create account
		modelAndView.setViewName("index");
		accountService.createAccount(account);
		log.debug("Created Account with name = " + account.getName() + ", id="
				+ account.getId());

		return modelAndView;
	}
	
	@RequestMapping(value="/{id}/lodgement", method = RequestMethod.GET)
	public String lodgement(ModelMap model, @PathVariable("id") String id) {
		log.debug("Showing lodgement page for account with id = " + id);
		Account account = this.accountService.getAccount(id);
		model.addAttribute("account", account);
		
		return "lodgement";
	}
	
	@RequestMapping(value="/{id}/lodgement", method = RequestMethod.POST)
	public String makeLodgement(@RequestParam("amount") Double amount, ModelMap model,
			@PathVariable("id") String id, Locale locale, RedirectAttributes redirectAttributes) {
		log.debug("Making lodgement for account with id = " + id);
		// Lookup Account
		Account account = this.accountService.getAccount(id);
		model.addAttribute("account", account);
		// Create and save Transaction
		try {
			this.accountService.makeTransaction(account, amount, TransactionType.LODGMENT);
		} catch (BalanceException e) {
			redirectAttributes.addFlashAttribute("ERR_MESSAGE", messageSource.getMessage(
					"lodgment.failure", new String[] {""+amount}, locale));
			return "redirect:/account/"+id;
		}
		
		redirectAttributes.addFlashAttribute("FLASH_MESSAGE", messageSource.getMessage(
				"lodgment.success", new String[] {""+amount}, locale));
		
		return "redirect:/account/"+id;
	}
	
	@RequestMapping(value="/{id}/withdrawal", method = RequestMethod.GET)
	public String withdrawal(ModelMap model, @PathVariable("id") String id) {
		log.debug("Showing withdrawal page for account with id = " + id);
		Account account = this.accountService.getAccount(id);
		model.addAttribute("account", account);
		
		return "withdrawal";
	}
	
	@RequestMapping(value="/{id}/withdrawal", method = RequestMethod.POST)
	public String makeWithdrawal(@RequestParam("amount") Double amount, ModelMap model, 
			@PathVariable("id") String id, Locale locale, RedirectAttributes redirectAttributes) {
		log.debug("Making withdrawal for account with id = " + id);
		Account account = this.accountService.getAccount(id);
		model.addAttribute("account", account);
		
		try {
			accountService.makeTransaction(account, amount, TransactionType.WITHDRAWAL);
		}catch (BalanceException e) {
			redirectAttributes.addFlashAttribute("ERR_MESSAGE", messageSource.getMessage(
					"withdrawal.balance.error", new String[] {""+account.getBalance(), ""+amount}, locale));
			return "redirect:/account/"+id;
		}
		
		redirectAttributes.addFlashAttribute("FLASH_MESSAGE", messageSource.getMessage(
				"withdrawal.success", new String[] {""+amount}, locale));
		
		return "redirect:/account/"+id;
	}
	
	@RequestMapping(value="/{id}/transfer", method = RequestMethod.GET)
	public String transfer(ModelMap model, @PathVariable("id") String id) {
		
		Account account = this.accountService.getAccount(id);
		model.addAttribute("account", account);
		
		return "transfer";
	}
	
	@RequestMapping(value="/{id}/transfer", method = RequestMethod.POST)
	public String makeTransfer(@RequestParam("amount") Double amount, @RequestParam("toAccount") String toAccountId, 
			ModelMap model, @PathVariable("id") String id, Locale locale, RedirectAttributes redirectAttributes) {
		
		Account account   = this.accountService.getAccount(id);
		Account toAccount = this.accountService.getAccount(toAccountId);
		
		if (toAccount == null) {
			redirectAttributes.addFlashAttribute("ERR_MESSAGE", messageSource.getMessage(
					"transfer.account.not.exists", new String[] {""+toAccountId}, locale));
			return "redirect:/account/"+id;
		}
		model.addAttribute("account", account);
		
		try {
			accountService.makeTransfer(account, toAccount, amount);
		}catch (BalanceException e) {
			redirectAttributes.addFlashAttribute("ERR_MESSAGE", messageSource.getMessage(
					"withdrawal.balance.error", new String[] {""+account.getBalance(), ""+amount}, locale));
			return "redirect:/account/"+id;
		}
		
		redirectAttributes.addFlashAttribute("FLASH_MESSAGE", messageSource.getMessage(
				"transfer.success", new String[] {""+amount, account.getId(), toAccount.getId()}, locale));
		
		return "redirect:/account/"+id;
	}
}
