package me.gralak.controllers;

import static org.junit.Assert.assertEquals;
import me.gralak.controllers.MainController;

import org.junit.Test;


public class MainControllerTest {
	
	@Test
	public void printWelcomeTest() {
		MainController mc = new MainController();
		assertEquals("index", mc.showIndex());
	}
}
