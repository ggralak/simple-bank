package me.gralak.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import me.gralak.enums.TransactionType;
import me.gralak.exceptions.BalanceException;
import me.gralak.model.Account;
import me.gralak.model.Transaction;
import me.gralak.service.AccountService;
import me.gralak.validators.AccountValidator;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.MessageSource;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

public class AccountControllerTest {
	
	private AccountController 	accountController;
	private AccountService 		accountService;
	private MessageSource 		messageSource;
	
	@InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(new AccountValidator());
    }
	
	@Before
	public void setUp() {
		accountController = new AccountController();
		
		accountService = mock(AccountService.class);
		accountController.accountService = accountService;
		
		messageSource = mock(MessageSource.class);
		accountController.messageSource = messageSource;
	}
	
	/**
	 * Tests if search is returning correct accounts in the model.
	 */
	@Test
	public void testSearch() {
		String   name  = "Doe";
		ModelMap model = new ModelMap();
		
		List<Account> accounts = new ArrayList<Account>();
		accounts.add(new Account());
		accounts.add(new Account());
		when(accountService.findAccountByName(name)).thenReturn(accounts );
				
		assertEquals("index", accountController.search(name, model));
		
		assertNotNull(model.get("accounts"));
		List<Account> returnedAccounts = (List<Account>)model.get("accounts");
		assertEquals(accounts.size(), returnedAccounts.size());
	}
	
	/**
	 * Tests if search is returning correct accounts in the model.
	 */
	@Test
	public void testShowAccount() {
		ModelMap model = new ModelMap();
		Account account = new Account();
		List<Transaction> transactions = new ArrayList<Transaction>();
		transactions.add(new Transaction());
		transactions.add(new Transaction());
		
		String id = "DOE001";
		account.setId(id );
		when(accountService.getAccount(id)).thenReturn(account);
		when(accountService.getTransactions(id)).thenReturn(transactions);
		
		assertEquals("account", accountController.showAccount(model, id));
		assertEquals(2, transactions.size());
		assertNotNull(model.get("account"));
		Account returnedAccount = (Account)model.get("account");
		assertEquals(account.getId(), returnedAccount.getId());
	}
	
	/**
	 * Tests if correct page is shown and 
	 * Account object as a model is returned 
	 * when an Account is being created.
	 */
	@Test
	public void testShowCreateAccount() {
		ModelAndView modelAndView = accountController.showCreateAccount();
		assertEquals("new-account", modelAndView.getViewName());
		assertEquals(Account.class, ((Account)modelAndView.getModelMap().get("account")).getClass());
	}
	
	/**
	 * Tests if Account is being create properly.
	 * Also checks if validation errors are being handled.
	 */
	@Test
	public void testCreateAccountWithErrors() {
		
		Account account = new Account();
		account.setId("DOE001");
		
		BindingResult result = mock(BindingResult.class);
		when(result.hasErrors()).thenReturn(true);
		
		ModelAndView modelAndView = accountController.createAccount(account, result);
		
		assertEquals("new-account", modelAndView.getViewName());
	}
	
	/**
	 * Tests if Account is being create properly
	 * when no errors occured
	 */
	@Test
	public void testCreateAccount() {
		
		Account account = new Account();
		account.setId("DOE001");
		
		BindingResult result = mock(BindingResult.class);
		when(result.hasErrors()).thenReturn(false);
		
		ModelAndView modelAndView = accountController.createAccount(account, result);
		// Make sure account has been created and flow returned to index page
		verify(accountService).createAccount(account);
		assertEquals("Should return to index page after account creation",
				"index", modelAndView.getViewName());
	}
	
	/**
	 * Make sure validator is initialized properly.
	 */
	@Test
	public void initBinder() {
		WebDataBinder binder = mock(WebDataBinder.class);
		accountController.initBinder(binder);
		
		verify(binder).setValidator(isA(AccountValidator.class));
	}
	
	/**
	 * Tests if Lodgement page is being shown.
	 */
	@Test
	public void testShowLodgementPage() {
		String   id    = "DOE001";
		ModelMap model = new ModelMap();
		
		assertEquals("lodgement", accountController.lodgement(model, id));
	}
	
	/**
	 * Tests if Lodgement is being made correctly.
	 * 
	 * @throws BalanceException 
	 */
	@Test
	public void testMakeLodgement() throws BalanceException {
		String   id    = "DOE001";
		ModelMap model = new ModelMap();
		Double amount  = new Double(100);
		Locale locale  = new Locale("en_IE");
		RedirectAttributes redirectAttributes = new RedirectAttributesModelMap();
		
		
		assertEquals("Correct Lodgement invocation should redirect to the account page",
				"redirect:/account/"+id, accountController.makeLodgement(amount, model, id, locale, redirectAttributes));
		Account account = (Account)model.get("account");
		verify(accountService).makeTransaction(account, amount, TransactionType.LODGMENT);
		
		assertFalse("Lodgement succeeded - shouldn't be any error message",
				redirectAttributes.getFlashAttributes().containsKey("ERR_MESSAGE"));
		assertTrue("Lodgement succeeded - should be flash messages",
				redirectAttributes.getFlashAttributes().containsKey("FLASH_MESSAGE"));
	}
	
	/**
	 * Tests if Lodgement is being made correctly. 
	 * In case AccountSerivce fails see if it is handled.
	 * 
	 * @throws BalanceException 
	 */
	@Test
	public void testMakeLodgementFail() throws BalanceException {
		String   id    = "DOE001";
		ModelMap model = new ModelMap();
		Double amount  = new Double(100);
		Locale locale  = new Locale("en_IE");
		RedirectAttributes redirectAttributes = new RedirectAttributesModelMap();
		
		Account account = new Account();
		account.setId(id);
		when(accountService.getAccount(id)).thenReturn(account );
		
		doThrow(BalanceException.class).when(accountService).makeTransaction(
				account, amount, TransactionType.LODGMENT);
		
		assertEquals("Something worng but still should go to the account page",
				"redirect:/account/"+id, accountController.makeLodgement(amount, model, id, locale, redirectAttributes));
		assertTrue("Lodgement failed - should be error message",
				redirectAttributes.getFlashAttributes().containsKey("ERR_MESSAGE"));
		assertFalse("Lodgement failed - shouldn't be any flash messages",
				redirectAttributes.getFlashAttributes().containsKey("FLASH_MESSAGE"));
	}
	
	/**
	 * Tests if Withdrawal page is being shown.
	 */
	@Test
	public void testShowWithdrawalPage() {
		String   id    = "DOE001";
		ModelMap model = new ModelMap();
		
		assertEquals("withdrawal", accountController.withdrawal(model, id));
	}
	
	/**
	 * Tests if Withdrawal is being made correctly.
	 * 
	 * @throws BalanceException 
	 */
	@Test
	public void testMakeWithdrawal() throws BalanceException {
		String   id    = "DOE001";
		ModelMap model = new ModelMap();
		Double amount  = new Double(100);
		Locale locale  = new Locale("en_IE");
		RedirectAttributes redirectAttributes = new RedirectAttributesModelMap();
		
		Account account = new Account();
		account.setId(id);
		when(accountService.getAccount(id)).thenReturn(account);
		
		assertEquals("Correct Withdrawal invocation should redirect to the account page",
				"redirect:/account/"+id, accountController.makeWithdrawal(amount, model, id, locale, redirectAttributes));
		
		verify(accountService).makeTransaction(account, amount, TransactionType.WITHDRAWAL);
		
		assertFalse("Withdrawal succeeded - shouldn't be any error message",
				redirectAttributes.getFlashAttributes().containsKey("ERR_MESSAGE"));
		assertTrue("Withdrawal succeeded - should be flash messages",
				redirectAttributes.getFlashAttributes().containsKey("FLASH_MESSAGE"));
	}
	
	/**
	 * Tests if Withdrawal is being made correctly.
	 * @throws BalanceException 
	 */
	@Test
	public void testMakeWithdrawalFail() throws BalanceException {
		String   id    = "DOE001";
		ModelMap model = new ModelMap();
		Double amount  = new Double(100);
		Locale locale  = new Locale("en_IE");
		RedirectAttributes redirectAttributes = new RedirectAttributesModelMap();
		
		Account account = new Account();
		account.setId(id);
		when(accountService.getAccount(id)).thenReturn(account);
		
		doThrow(BalanceException.class).when(accountService).makeTransaction(
				account, amount, TransactionType.WITHDRAWAL);
		
		assertEquals("Something worong but still should come back to the account page",
				"redirect:/account/"+id, accountController.makeWithdrawal(amount, model, id, locale, redirectAttributes));
		assertTrue("Withdrawal failed - should be error message",
				redirectAttributes.getFlashAttributes().containsKey("ERR_MESSAGE"));
		assertFalse("Withdrawal failed - shouldn't be any flash messages",
				redirectAttributes.getFlashAttributes().containsKey("FLASH_MESSAGE"));
	}
	
	/**
	 * Tests if Transfer page is being shown.
	 */
	@Test
	public void testShowTransferPage() {
		String   id    = "DOE001";
		ModelMap model = new ModelMap();
		
		assertEquals("transfer", accountController.transfer(model, id));
	}
	
	/**
	 * Make sure making transfer is handled correctly.
	 * @throws BalanceException 
	 */
	@Test
	public void testMakeTransfer() throws BalanceException {
		String   id          = "DOE001";
		String   toAccountId = "DOE002";
		ModelMap model       = new ModelMap();
		Double amount        = new Double(100);
		Locale locale        = new Locale("en_IE");
		RedirectAttributes redirectAttributes = new RedirectAttributesModelMap();
		
		Account account   = new Account();
		Account toAccount = new Account();
		
		account.setId(id);
		account.setId(toAccountId);
		
		when(accountService.getAccount(id)).thenReturn(account);
		when(accountService.getAccount(toAccountId)).thenReturn(toAccount);
		
		assertEquals("Correct Transfer invocation should redirect to the account page",
				"redirect:/account/"+id, accountController.makeTransfer(amount, toAccountId, model, id, locale, redirectAttributes));
		
		verify(accountService).makeTransfer(account, toAccount, amount);
		
		assertFalse("Transfer succeeded - shouldn't be any error message",
				redirectAttributes.getFlashAttributes().containsKey("ERR_MESSAGE"));
		assertTrue("Transfer succeeded - should be flash messages",
				redirectAttributes.getFlashAttributes().containsKey("FLASH_MESSAGE"));
	}
	
	/**
	 * Make sure that overdraft result in error.
	 * @throws BalanceException 
	 */
	@Test
	public void testMakeTransferFail() throws BalanceException {
		String   id          = "DOE001";
		String   toAccountId = "DOE002";
		ModelMap model       = new ModelMap();
		Double amount        = new Double(100);
		Locale locale        = new Locale("en_IE");
		RedirectAttributes redirectAttributes = new RedirectAttributesModelMap();
		
		Account account   = new Account();
		Account toAccount = new Account();
		
		account.setId(id);
		account.setId(toAccountId);
		
		when(accountService.getAccount(id)).thenReturn(account);
		when(accountService.getAccount(toAccountId)).thenReturn(toAccount);
		
		doThrow(BalanceException.class).when(accountService).makeTransfer(account, toAccount, amount);
		
		assertEquals("Correct Transfer invocation should redirect to the account page",
				"redirect:/account/"+id, accountController.makeTransfer(amount, toAccountId, model, id, locale, redirectAttributes));
		
		// Error is during making transfer so it should be invoked.
		verify(accountService).makeTransfer(account, toAccount, amount);
		
		assertTrue("Transfer failed - should be error message",
				redirectAttributes.getFlashAttributes().containsKey("ERR_MESSAGE"));
		assertFalse("Transfer succeeded - shouldn't be any flash messages",
				redirectAttributes.getFlashAttributes().containsKey("FLASH_MESSAGE"));
	}
	
	/**
	 * Make sure making transfer is handled correctly.
	 * @throws BalanceException 
	 */
	@Test
	public void testMakeTransferToNonExistingAccound() throws BalanceException {
		String   id          = "DOE001";
		String   toAccountId = "DOE002";
		ModelMap model       = new ModelMap();
		Double amount        = new Double(100);
		Locale locale        = new Locale("en_IE");
		RedirectAttributes redirectAttributes = new RedirectAttributesModelMap();
		
		Account account = new Account();
		account.setId(id);
		when(accountService.getAccount(id)).thenReturn(account);
		when(accountService.getAccount(toAccountId)).thenReturn(null);
		System.out.println(accountService.getAccount(toAccountId));
		assertEquals("Should redirect to the account page",
				"redirect:/account/"+id, accountController.makeTransfer(amount, toAccountId, model, id, locale, redirectAttributes));
		
		// Make sure makeTransaction was not invoked.
		verify(accountService, never()).makeTransaction(account, amount, TransactionType.TRANSFER);
		
		assertTrue("Transfer failed - should be error message",
				redirectAttributes.getFlashAttributes().containsKey("ERR_MESSAGE"));
		assertFalse("Transfer failed - shouldn't be any flash messages",
				redirectAttributes.getFlashAttributes().containsKey("FLASH_MESSAGE"));
	}
}
