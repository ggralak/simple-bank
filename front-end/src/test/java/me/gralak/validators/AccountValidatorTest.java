package me.gralak.validators;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

import me.gralak.model.Account;

import org.junit.Test;
import org.springframework.validation.DirectFieldBindingResult;
import org.springframework.validation.Errors;

/**
 * Test for {@link AccountValidator}
 * 
 * @author Grzegorz Gralak
 */
public class AccountValidatorTest {
	
	/**
	 * Tests supports of the AccountValidator
	 */
	@Test
	public void testSupports() {
		AccountValidator accountValidator = new AccountValidator();
		
		assertFalse("AccountValidator cannot support String class", 
				accountValidator.supports(String.class));
		
		assertTrue("AccountValidator must support Accoun class",
				accountValidator.supports(Account.class));
	}
	
	/**
	 * Account object not populated - validation should fail.
	 */
	@Test
	public void testValidateFail() {
		AccountValidator accountValidator = new AccountValidator();
		Account account = new Account();
		Errors e = new DirectFieldBindingResult(account, "account");
		//when(e.getFieldValue("id")).thenReturn(null);
		accountValidator.validate(account, e);
		assertTrue(e.hasErrors());
	}
	
	/**
	 * Account object populated - validation should succeed.
	 */
	@Test
	public void testValidateSuccess() {
		AccountValidator accountValidator = new AccountValidator();
		Account account = new Account();
		account.setId("DOE001");
		account.setName("John Doe");
		account.setAddress("Address");
		account.setPhoneNo("01234");
		Errors e = new DirectFieldBindingResult(account, "account");
		accountValidator.validate(account, e);
		assertFalse(e.hasErrors());
	}
}
