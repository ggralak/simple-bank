package me.gralak.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import me.gralak.model.Account;
import me.gralak.model.Transaction;

import org.junit.Before;
import org.junit.Test;

public class TransactionDaoImplTest {
	private EntityManager em;
	private TransactionDaoImpl transactionDao;

	@Before
	public void setUp() {
		em = mock(EntityManager.class);
		transactionDao = new TransactionDaoImpl();
		transactionDao.em = em;
	}

	/**
	 * Checks if appropriate List of Transactions is returned.
	 */
	@Test
	public void testGetAllTransactions() {
		List<Transaction> transactions = new ArrayList<Transaction>();
		transactions.add(new Transaction());
		transactions.add(new Transaction());
		String accountId = "DOE001";
		
		// Mock Query and Entity Manager
		Query query = mock(Query.class);
		when(query.setParameter("accountId", accountId)).thenReturn(query);
		
		when(query.getResultList()).thenReturn(transactions );
		when(em.createQuery(anyString())).thenReturn(query);
		
		List<Transaction> returnedTransactions = transactionDao.getAllTransactions(accountId);
		assertNotNull(returnedTransactions);
		assertEquals(2, returnedTransactions.size());
	}
}
