package me.gralak.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.persistence.EntityManager;

import me.gralak.model.Account;

import org.junit.Before;
import org.junit.Test;

/**
 * Tests {@link GenericDaoImpl}.
 * 
 * @author Grzegorz Gralak
 */
public class GenericDaoImplTest {
	private EntityManager  em;
	private AccountDaoImpl accountDao;
	
	@Before
	public void setUp(){
		em = mock(EntityManager.class);
		accountDao = new AccountDaoImpl();
		accountDao.em = em;
	}
	
	/**
	 * Make sure EntityManager's persist is invoked.
	 */
	@Test
	public void testSave() {
		Account account = new Account();
		accountDao.save(account );
		
		verify(em).persist(account);
	}
	
	/**
	 * Make sure EntityManager's merge is invoked.
	 */
	@Test
	public void testUpdate() {
		Account account = new Account();
		accountDao.update(account );
		
		verify(em).merge(account);
	}
	
	/**
	 * Make sure EntityManager's find is used to lookup 
	 * entity with given id.
	 */
	@Test
	public void testFind() {
		String id = "DOE001";
		Account account = new Account();
		when(em.find(Account.class, id)).thenReturn(account );
		
		assertEquals(account,accountDao.find(id));
	}
}
