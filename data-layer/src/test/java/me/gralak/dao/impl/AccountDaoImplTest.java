package me.gralak.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import me.gralak.model.Account;

import org.junit.Before;
import org.junit.Test;

public class AccountDaoImplTest {
	private EntityManager  em;
	private AccountDaoImpl accountDao;
	
	@Before
	public void setUp(){
		em = mock(EntityManager.class);
		accountDao = new AccountDaoImpl();
		accountDao.em = em;
	}
	
	/**
	 * Checks if appropriate List of Accounts is returned.
	 */
	@Test
	public void testFindByName() {
		// Prepare data
		List<Account> accounts = new ArrayList<Account>();
		Account account = new Account();
		account.setName("John Doe");
		accounts.add(account);
		String name = "DOE";
		
		// Mock Query and Entity Manager
		Query query = mock(Query.class);
		when(query.setParameter("name", name )).thenReturn(query);
		when(query.getResultList()).thenReturn(accounts);
		when(em.createQuery(anyString())).thenReturn(query );
		
		// Check if appropriate values are returned
		List<Account> returnedAccounts = accountDao.findByName(name);
		assertNotNull(returnedAccounts);
		assertEquals(accounts.size(), returnedAccounts.size());
		assertEquals("John Doe", returnedAccounts.get(0).getName());
	}
}
