package me.gralak.model;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class AccountTest {

	/**
	 * I wouldn't do this but been asked for 100% test coverage...
	 */
	@Test
	public void testSettersGetters() {
		Account account = new Account();
		account.setId("DOE001");
		account.setName("John Doe");
		account.setBalance(new Double(200));
		account.setPhoneNo("01234");
		account.setAddress("Here");
		List<Transaction> transactions = new ArrayList<Transaction>();
		transactions.add(new Transaction());
		account.setTransactions(transactions );
		
		assertEquals("DOE001", account.getId());
		assertEquals("John Doe", account.getName());
		assertEquals(new Double(200), account.getBalance());
		assertEquals("01234", account.getPhoneNo());
		assertEquals("Here", account.getAddress());
		assertEquals(1, account.getTransactions().size());
	}
}
