package me.gralak.model;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import me.gralak.enums.TransactionType;

import org.junit.Test;

public class TransactionTest {

	/**
	 * I wouldn't do this but been asked for 100% test coverage...
	 */
	@Test
	public void testSettersGetters() {
		
		Account account = new Account();
		Account fromAccount = new Account();
		Account toAccount = new Account();
		Date date = new Date();
		
		
		Transaction transaction = new Transaction();
		transaction.setId(1);
		transaction.setAmount(new Double(100));
		transaction.setDate(date);
		transaction.setType(TransactionType.LODGMENT);
		transaction.setAccount(account );
		transaction.setFromAccount(fromAccount );
		transaction.setToAccount(toAccount);
		
		assertEquals(1, transaction.getId());
		assertEquals(new Double(100), transaction.getAmount());
		assertEquals(date, transaction.getDate());
		assertEquals(TransactionType.LODGMENT, transaction.getType());
		assertEquals(account, transaction.getAccount());
		assertEquals(fromAccount, transaction.getFromAccount());
		assertEquals(toAccount, transaction.getToAccount());
	}
}
