insert into account(account_id, balance, name, address, phone_no) values ('DOE001', 400, 'John Doe', '3, Parnell St, Dublin', '01234');
insert into account(account_id, balance, name, address, phone_no) values ('DOE002', 300, 'John Doe2','4, Parnell St, Dublin', '01234');

INSERT INTO transaction(transaction_id, acc_id, to_acc_id, amount, type, date) 
VALUES (1, 'DOE001', NULL, 400, 'LODGMENT', TO_TIMESTAMP('10/07/2013 10:30:00', 'DD/MM/YYYY HH:MI:SS'));

INSERT INTO transaction(transaction_id, acc_id, to_acc_id, amount, type, date) 
VALUES (2, 'DOE002', NULL, 300, 'LODGMENT', TO_TIMESTAMP('11/07/2013 11:20:00', 'DD/MM/YYYY HH:MI:SS'));