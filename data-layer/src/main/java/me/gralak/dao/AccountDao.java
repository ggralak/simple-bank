package me.gralak.dao;

import java.util.List;

import me.gralak.model.Account;

/**
 * Data Access Object for Account model.
 * Apart from generic dao methods it defines extra functionality.
 * 
 * @author Grzegorz Gralak
 *
 */
public interface AccountDao extends GenericDao<Account>{
	
	/**
	 * Look up all Account objects which have name property containing <code>name</code> parameter.
	 * 
	 * @param name will be used in LIKE style comparison
	 * @return Account objects which have name property containing <code>name</code> parameter
	 */
	public List<Account> findByName(String name);
}
