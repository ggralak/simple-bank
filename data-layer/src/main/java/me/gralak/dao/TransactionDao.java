package me.gralak.dao;

import java.util.List;

import me.gralak.model.Transaction;

public interface TransactionDao extends GenericDao<Transaction> {
	List<Transaction> getAllTransactions(String accountId);
}
