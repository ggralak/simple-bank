package me.gralak.dao;

/**
 * Defines all common DAO methods to share between all DAO implementations.
 * 
 * @author Grzegorz Gralak
 *
 * @param <T>
 */
public interface GenericDao<T> {
	
	/**
	 * Save object of the type T to database
	 * @param object
	 */
	void save(T object);
	
	/**
	 * Find object of the type T with the given id
	 * @param id
	 * @return
	 */
	T find(Object id);
	
	/**
	 * Update object of the type T
	 * @param t
	 * @return
	 */
	T update(T t);
}
