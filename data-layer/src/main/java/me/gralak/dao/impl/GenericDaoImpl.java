package me.gralak.dao.impl;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import me.gralak.dao.GenericDao;

import org.springframework.transaction.annotation.Transactional;


/**
 * Implementation of the {@link GenericDao}.
 * 
 * @author Grzegorz Gralak
 *
 * @param <T> type which will be used to work with repository
 */
public abstract class GenericDaoImpl<T> implements GenericDao<T>{
	
	private Class<T> type;
	
	public GenericDaoImpl() {
        Type t = getClass().getGenericSuperclass();
        ParameterizedType pt = (ParameterizedType) t;
        type = (Class) pt.getActualTypeArguments()[0];
    }
	
	@PersistenceContext(unitName = "SimpleBankUnit")
    protected EntityManager em;
	
	/*
	 * Common DAO methods.
	 */
	
	@Override
	@Transactional
	public void save(T object) {
		this.em.persist(object);
	}

	@Override
	public T find(Object id) {
		return (T) this.em.find(type, id);
	}

	@Override
	public T update(T t) {
		return this.em.merge(t);
	}

}
