package me.gralak.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import me.gralak.dao.TransactionDao;
import me.gralak.model.Transaction;

@Repository("transactionDao")
public class TransactionDaoImpl extends GenericDaoImpl<Transaction> implements TransactionDao {

	@Override
	public List<Transaction> getAllTransactions(String accountId) {
		Query query = this.em
                .createQuery("select t FROM Transaction t where t.account.id = :accountId");
        query.setParameter("accountId", accountId);
        List<Transaction> transactions = query.getResultList();
        
		return transactions;
	}

}
