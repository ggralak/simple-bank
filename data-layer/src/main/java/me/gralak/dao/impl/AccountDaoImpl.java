package me.gralak.dao.impl;

import java.util.List;

import javax.persistence.Query;

import me.gralak.dao.AccountDao;
import me.gralak.model.Account;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

@Repository("accountDao")
public class AccountDaoImpl extends GenericDaoImpl<Account> implements AccountDao {
	static Logger log = Logger.getLogger(AccountDaoImpl.class.getSimpleName());

	@Override
	public List<Account> findByName(String name) {
		log.warn("name = " + name);
		Query query = this.em
                .createQuery("select a FROM Account a where upper(a.name) like upper(:name)");
        query.setParameter("name", "%"+name+"%");
        List<Account> accounts = query.getResultList();
        
		return accounts;
	}

}
