package me.gralak.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import me.gralak.enums.TransactionType;

@Entity
@Table(name="transaction")
public class Transaction {
	private long 			id;
	private Account 		account;
	private Account 		toAccount;
	private Account 		fromAccount;
	private Double 			amount;
	private TransactionType type;
	private Date			date;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="transaction_id", nullable=false)
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	@ManyToOne(cascade=CascadeType.REFRESH ,fetch=FetchType.EAGER)
	@JoinColumn(name="acc_id")
	public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}
	@ManyToOne(cascade=CascadeType.REFRESH ,fetch=FetchType.EAGER)
	@JoinColumn(name="to_acc_id")
	public Account getToAccount() {
		return toAccount;
	}
	public void setToAccount(Account toAccount) {
		this.toAccount = toAccount;
	}
	@ManyToOne(cascade=CascadeType.REFRESH ,fetch=FetchType.EAGER)
	@JoinColumn(name="from_acc_id")
	public Account getFromAccount() {
		return fromAccount;
	}
	public void setFromAccount(Account fromAccount) {
		this.fromAccount = fromAccount;
	}
	@Column(name="amount", nullable=false)
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	@Enumerated(EnumType.STRING)
	@Column(name="type", nullable=false)
	public TransactionType getType() {
		return type;
	}
	public void setType(TransactionType type) {
		this.type = type;
	}
	@Column(name="date")
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
}
