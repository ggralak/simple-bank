package me.gralak.enums;

public enum TransactionType {
	LODGMENT,
	WITHDRAWAL,
	TRANSFER;
}
