package me.gralak.exceptions;

/**
 * Marker exception for situations with Account Balance.
 * 
 * @author Grzegorz Gralak
 */
public class BalanceException extends Exception {

}
