package me.gralak.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import me.gralak.dao.AccountDao;
import me.gralak.dao.TransactionDao;
import me.gralak.enums.TransactionType;
import me.gralak.exceptions.BalanceException;
import me.gralak.model.Account;
import me.gralak.model.Transaction;
import me.gralak.service.AccountService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of the {@link AccountService}.
 * 
 * @author Grzegorz Gralak
 */
@Service("accountService")
public class AccountServiceImpl implements AccountService {

	@Resource
	@Qualifier("accountDao")
	AccountDao accountDao;
	
	@Resource
	@Qualifier("transactionDao")
	private TransactionDao transactionDao;

	@Override
	@Transactional
	public List<Account> findAccountByName(String name) {
		return accountDao.findByName(name);
	}

	@Override
	@Transactional
	public Account getAccount(String id) {
		return accountDao.find(id);
	}
	
	@Override
	@Transactional
	public void createAccount(Account account) {
		// Make sure balance is always 0
		account.setBalance(new Double(0));
		accountDao.save(account);
	}
	
	@Override
	@Transactional
	public List<Transaction> getTransactions(String accountId) {
		return transactionDao.getAllTransactions(accountId);
	}
	
	@Override
	@Transactional
	public void makeTransaction(Account account, Double amount,
			TransactionType transactionType) throws BalanceException {
		// First increase or decrease Account Balance.
		if (TransactionType.LODGMENT.equals(transactionType)) {
			increaseBalance(amount, account);
		} else if (TransactionType.WITHDRAWAL.equals(transactionType)) {
			decreaseBalance(amount, account);
		}

		// Then create Transaction in the system.
		Transaction transaction = new Transaction();
		transaction.setAccount(account);
		transaction.setDate(new Date());
		transaction.setType(transactionType);
		transaction.setAmount(amount);
		this.transactionDao.save(transaction);
	}
	
	@Override
	@Transactional
	public void makeTransfer(Account fromAccount, Account toAccount,
			Double amount) throws BalanceException {
		decreaseBalance(amount, fromAccount);
		increaseBalance(amount, toAccount);
		
		// Then create Transfer Transaction coming out the origin account
		Transaction transaction = new Transaction();
		transaction.setAccount(fromAccount);
		transaction.setToAccount(toAccount);
		transaction.setDate(new Date());
		transaction.setType(TransactionType.TRANSFER);
		transaction.setAmount(amount);
		this.transactionDao.save(transaction);
		
		// And Transfer Transaction coming in to the destination account
		transaction = new Transaction();
		transaction.setAccount(toAccount);
		transaction.setFromAccount(fromAccount);
		transaction.setDate(new Date());
		transaction.setType(TransactionType.TRANSFER);
		transaction.setAmount(amount);
		this.transactionDao.save(transaction);
	}
	
	@Transactional
	private void increaseBalance(Double amount, Account account) {
		account.setBalance(account.getBalance() + amount);
		this.accountDao.update(account);
	}

	@Transactional
	private void decreaseBalance(Double amount, Account account)
			throws BalanceException {
		if (account.getBalance() - amount < 0) {
			// This will roll back any other changes as propagation is REQUIRED
			// by default.
			throw new BalanceException();
		}
		account.setBalance(account.getBalance() - amount);
		accountDao.update(account);
	}
	
	public void setAccountDao(AccountDao accountDao) {
		this.accountDao = accountDao;
	}
	
	public void setTransactionDao(TransactionDao transactionDao) {
		this.transactionDao = transactionDao;
	}
	
	
}
