package me.gralak.service;

import java.util.List;

import me.gralak.enums.TransactionType;
import me.gralak.exceptions.BalanceException;
import me.gralak.model.Account;
import me.gralak.model.Transaction;

import org.springframework.transaction.annotation.Transactional;

/**
 * Account Service - holds all of the business logic functionality
 * wrapped in the transactions so they're atomic.
 * 
 * @author Grzegorz Gralak
 */
public interface AccountService {
	
	/**
	 * Find all Accounts which contains name.
	 * @param name
	 * @return
	 */
	@Transactional
	List<Account> findAccountByName(String name);
	
	/**
	 * Find account with given id
	 * @param id
	 * @return
	 */
	@Transactional
	Account getAccount(String id);
	
	/**
	 * Create Account
	 * @param account
	 */
	@Transactional
	void createAccount(Account account);
	
	/**
	 * Returns all transaction for the given account.
	 * 
	 * @param accountId - used to lookup transactions
	 * @return List of all Transactions for the given account
	 */
	List<Transaction> getTransactions(String accountId);
	
	/**
	 * Creates transaction in the system based on passed data.
	 * Depending on the type of the Transaction it can be:
	 * <ul>
	 * <li>LODGMENT - for the given account</li>
	 * <li>WITHDRAWAL - for the given account</li>
	 * </ul>
	 * 
	 * @param account         - Account where transaction will be made
	 * @param amount          - Amount of the Transaction
	 * @param transactionType - Type of the Transaction
	 */
	void makeTransaction(Account account, Double amount, TransactionType transactionType)  throws BalanceException;
	
	/**
	 * Creates transfer transaction between two accounts.
	 * 
	 * @param fromAccount     - account where money are being transferred from
	 * @param toAccount       - account where money are being transferred to
	 * @param amount          - Amount of the Transaction
	 */
	void makeTransfer(Account fromAccount, Account toAccount, Double amount)  throws BalanceException;
}
