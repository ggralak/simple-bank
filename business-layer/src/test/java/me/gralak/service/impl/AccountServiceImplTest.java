package me.gralak.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import me.gralak.dao.AccountDao;
import me.gralak.dao.TransactionDao;
import me.gralak.enums.TransactionType;
import me.gralak.exceptions.BalanceException;
import me.gralak.model.Account;
import me.gralak.model.Transaction;

import org.hamcrest.core.IsAnything;
import org.junit.Before;
import org.junit.Test;

/**
 * Tests AccounService implementation
 * 
 * @author Grzegorz Gralak
 */
public class AccountServiceImplTest {
	private AccountServiceImpl 	service;
	private AccountDao 			accountDao;
	private TransactionDao		transactionDao;
	
	@Before
	public void setUp(){
		service 	= new AccountServiceImpl();
		accountDao 	= mock(AccountDao.class);
		transactionDao = mock(TransactionDao.class);
		
		service.setAccountDao(accountDao);
		service.setTransactionDao(transactionDao);
	}
	
	/**
	 * Tests if correct Account is returned by AccounService.getAccount()
	 */
	@Test
	public void testFindAccountById() {
		Account account = new Account();
		String id = "DOE001";
		when(accountDao.find(id)).thenReturn(account);
		
		assertEquals("Wrong Account returned by AccountService",
				account, service.getAccount(id));
	}
	
	/**
	 * Tests if correct number of accounts is returned 
	 * by AccountService.findAccountByName(String)
	 */
	@Test
	public void testFindAccountByName() {
		String name = "Doe";
		List<Account> accounts = new ArrayList<Account>();
		accounts.add(new Account());
		accounts.add(new Account());
		when(accountDao.findByName(name)).thenReturn(accounts);
		
		assertEquals("Wrong number of accounts returned from AccountService",
				2, service.findAccountByName(name).size());
	}
	
	@Test
	public void testCreateAccount() {
		Account account = new Account();
		service.createAccount(account);
		
		// Verify that Account is saved by dao and balance is 0.
		verify(accountDao).save(account);
		assertEquals("Balance is not 0 when Account is created.",
				new Double(0), account.getBalance());
	}
	
	/**
	 * Tests if transactions are being lookup up correctly.
	 */
	@Test
	public void testGetTransactions() {
		String accountId = "DOE001";
		List<Transaction> transactions = new ArrayList<Transaction>();
		transactions.add(new Transaction());
		transactions.add(new Transaction());
		
		when(transactionDao.getAllTransactions(accountId)).thenReturn(transactions);
		
		List<Transaction> returnedTransactions = service.getTransactions(accountId);
		assertEquals(transactions.size(), returnedTransactions.size());
	}
	
	/**
	 * Check if lodgement is being processed correctly.
	 * Balance should be increased and Transaction saved.
	 */
	@Test
	public void testMakeLodgement() {
		Account account = new Account();
		account.setBalance(new Double(100));
		
		Double amount = new Double(100);
		
		try {
			service.makeTransaction(account, amount, TransactionType.LODGMENT);
		} catch (BalanceException e) {
			fail("During Lodgement there shouldn't be any BalanceException.");
		}
		
		assertEquals("Balance should be increased from 100 to 200",
				new Double(200), account.getBalance());
		verify(accountDao).update(account);
		// Make sure transaction was saved
		verify(transactionDao).save(isA(Transaction.class));
	}
	
	/**
	 * Check if too big withdrawal is being handled properly:
	 * BalanceException should be thrown.
	 * 
	 * @throws BalanceException - expected exception 
	 */
	@Test(expected=BalanceException.class)
	public void testMakeWithdrawWithOverdraft() throws BalanceException {
		Account account = new Account();
		account.setBalance(new Double(100));
		
		Double amount = new Double(200);
		
		service.makeTransaction(account, amount, TransactionType.WITHDRAWAL);
	}
	
	/**
	 * Check if withdrawal is being processed correctly.
	 * Balance should be decreased and Transaction saved.
	 */
	@Test
	public void testMakeWithdrawal() {
		Account account = new Account();
		account.setBalance(new Double(200));
		
		Double amount = new Double(100);
		
		try {
			service.makeTransaction(account, amount, TransactionType.WITHDRAWAL);
		} catch (BalanceException e) {
			fail("No overdraft, BalanceException not expected.");
		}
		
		assertEquals("Balance should be decreased from 200 to 100",
				new Double(100), account.getBalance());
		verify(accountDao).update(account);
		// Make sure transaction was saved
		verify(transactionDao).save(isA(Transaction.class));
	}
	
	/**
	 * Check if transfer is being processed correctly.
	 * Balance on the 'from' account should decrease
	 * and on the 'to' account should increase.
	 * Also 2 Transactions should be created.
	 */
	@Test
	public void testMakeTransfer() {
		
		Account fromAccount = new Account();
		Account toAccount   = new Account();
		Double  amount      = new Double(100);
		
		fromAccount.setBalance(new Double(400));
		toAccount.setBalance(new Double(400));
		
		try {
			service.makeTransfer(fromAccount, toAccount, amount);
		} catch (BalanceException e) {
			fail("No overdraft, BalanceException not expected.");
		}
		
		assertEquals(new Double(300), fromAccount.getBalance());
		assertEquals(new Double(500), toAccount.getBalance());
		
		verify(accountDao).update(fromAccount);
		verify(accountDao).update(toAccount);
		
		// Make sure transaction was saved
		verify(transactionDao, times(2)).save(isA(Transaction.class));
		
	}
	
	/**
	 * Check if too big transfer is being handled properly:
	 * BalanceException should be thrown.
	 * 
	 * @throws BalanceException - expected exception 
	 */
	@Test(expected=BalanceException.class)
	public void testMakeTransferWithOverdraft() throws BalanceException {
		Account fromAccount = new Account();
		Account toAccount   = new Account();
		Double  amount      = new Double(500);
		
		fromAccount.setBalance(new Double(400));
		toAccount.setBalance(new Double(400));
		
		service.makeTransfer(fromAccount, toAccount, amount);
		
	}
}
